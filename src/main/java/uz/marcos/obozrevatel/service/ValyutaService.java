package uz.marcos.obozrevatel.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.stereotype.Service;
import uz.marcos.obozrevatel.entity.Valyuta;
import uz.marcos.obozrevatel.model.ValyutaResult;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

@Service
public class ValyutaService {
   public ValyutaResult getValyuta(){
       ValyutaResult valyutaResult =new ValyutaResult();
       Gson gson = new Gson();
       URL url;
       try {
           url= new URL("http://cbu.uz/uzc/arkhiv-kursov-valyut/json/");
           URLConnection connection =url.openConnection();
           BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
           Type type =new TypeToken<ArrayList<Valyuta>>(){}.getType();
           ArrayList<Valyuta> resultVal = gson.fromJson(reader,type);
           for (Valyuta result: resultVal) {
               if (result.getCcy().equals("USD")){
                valyutaResult.setUsd(result.getRate());
                valyutaResult.setDiff1(result.getDiff());
               }




               if (result.getCcy().equals("GBP")){
                   valyutaResult.setGbp(result.getRate());
                   valyutaResult.setDiff2(result.getDiff());

               }
               if (result.getCcy().equals("EUR")){
                   valyutaResult.setEur(result.getRate());
                   valyutaResult.setDiff3(result.getDiff());
               }
               if (result.getCcy().equals("RUB")){
                   valyutaResult.setRub(result.getRate());
                   valyutaResult.setDiff4(result.getDiff());
               }
           }
           return valyutaResult;
       } catch (MalformedURLException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       }
       return null;
   }

}
