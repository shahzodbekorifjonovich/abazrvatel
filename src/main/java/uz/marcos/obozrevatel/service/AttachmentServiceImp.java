package uz.marcos.obozrevatel.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.marcos.obozrevatel.entity.Attachment;
import uz.marcos.obozrevatel.entity.AttachmentContent;
import uz.marcos.obozrevatel.entity.PostAppend;
import uz.marcos.obozrevatel.model.Result;
import uz.marcos.obozrevatel.repository.AttachmentContentRepository;
import uz.marcos.obozrevatel.repository.AttachmentRepository;
import uz.marcos.obozrevatel.repository.PostAppendRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Service
public class AttachmentServiceImp implements AttachmentService {
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;
    @Autowired
    PostAppendRepository postAppendRepository;
    @Override
    public Result save(MultipartFile file, HttpServletRequest request) {
        Result result=new Result();
        Cookie[] cookies=request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("postId")){
                result.setMessage(cookie.getValue());
                break;
            }
        }
        try{
            Attachment attachment=new Attachment();
            attachment.setName(UUID.randomUUID().toString());
            attachment.setContentType(file.getContentType());
            attachment.setOriginalName(file.getOriginalFilename());
            attachment.setSize(file.getSize());
            AttachmentContent content=new AttachmentContent();
            content.setContent(file.getBytes());
            Attachment attachment1= attachmentRepository.save(attachment);
            content.setAttachment(attachment1);
            attachmentContentRepository.save(content);

            PostAppend postAppend=new PostAppend();
            postAppend.setName("img");
            postAppend.setPostId(result.getMessage());
            postAppend.setAppendId(attachment1.getId());
            postAppendRepository.save(postAppend);
             result.setSuccess(true);

        } catch (IOException e) {
            e.printStackTrace();

        }
        return result;
    }

    @Override
    public List<Attachment> postAttachments(Integer postId) {
        return null;
    }
    @Override
    public byte[] getFile(Integer id, HttpServletResponse response) {
        Attachment attachment=attachmentRepository.getOne(id);
        AttachmentContent attachmentContent=attachmentContentRepository.findByAttachmentId(id);
        response.setHeader("Content-type", attachment.getContentType());
        response.setStatus(HttpStatus.OK.value());
        return attachmentContent.getContent();
    }
}
