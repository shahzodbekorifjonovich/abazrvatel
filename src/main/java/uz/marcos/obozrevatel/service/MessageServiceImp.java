package uz.marcos.obozrevatel.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.marcos.obozrevatel.entity.Message;
import uz.marcos.obozrevatel.repository.MessageRepository;

import javax.servlet.http.HttpServletRequest;

@Service
public class MessageServiceImp implements MessageService{
    @Autowired
    MessageRepository messageRepository;
    @Override
    public Message savedMessages(HttpServletRequest request) {
        Message message = new Message();
        message.setName(request.getParameter("username"));
        message.setPhoneNumber(request.getParameter("phoneNumber"));
        message.setText(request.getParameter("text"));
        messageRepository.save(message);
        return message;
    }

}
