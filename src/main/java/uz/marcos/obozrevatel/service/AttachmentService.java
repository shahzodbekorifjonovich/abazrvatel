package uz.marcos.obozrevatel.service;

import org.springframework.web.multipart.MultipartFile;
import uz.marcos.obozrevatel.entity.Attachment;
import uz.marcos.obozrevatel.model.Result;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface AttachmentService {
    public Result save(MultipartFile file, HttpServletRequest request);
    public List<Attachment> postAttachments(Integer postId);
    public  byte[] getFile(Integer id, HttpServletResponse response);
}
