package uz.marcos.obozrevatel.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.marcos.obozrevatel.entity.Role;
import uz.marcos.obozrevatel.entity.User;
import uz.marcos.obozrevatel.repository.RoleRepository;
import uz.marcos.obozrevatel.repository.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class UserServiceImp implements UserService{
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RoleRepository  roleRepository;
    @Override
    public User savedUser(HttpServletRequest request) {
        User user = new User();
        try{
            List<Role> roles=roleRepository.findAllByName("ROLE_USER");
        user.setFullname(request.getParameter("fullname"));
        user.setEmail(request.getParameter("email"));
        user.setRoles(roles);
        user.setUsername(request.getParameter("username"));
        user.setPassword(passwordEncoder.encode(request.getParameter("newPassword")));
        if (request.getParameter("newPassword").equals(request.getParameter("prePassword"))) {
           return userRepository.save(user);
        }
        }
        catch (Exception e){

        }
        return null;
    }

}
