package uz.marcos.obozrevatel.service;

import org.springframework.stereotype.Service;
import uz.marcos.obozrevatel.model.DataCalendar;

import java.util.Calendar;
import java.util.Locale;

@Service
public class CalendaService {
    public DataCalendar getData() {
        DataCalendar dataCalendar = new DataCalendar();
        Locale locale = new Locale("ru");
        Calendar calendar = Calendar.getInstance();
        dataCalendar.setDay(calendar.get(Calendar.DAY_OF_MONTH));
        dataCalendar.setMonth(calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT,Locale.ENGLISH));
        dataCalendar.setYear(calendar.get(Calendar.YEAR));
        return dataCalendar;
    }
}