package uz.marcos.obozrevatel.service;

import uz.marcos.obozrevatel.entity.User;
import uz.marcos.obozrevatel.model.Result;

import javax.servlet.http.HttpServletRequest;

public interface CommentService {
public Result save(HttpServletRequest request, User user, String text);


}
