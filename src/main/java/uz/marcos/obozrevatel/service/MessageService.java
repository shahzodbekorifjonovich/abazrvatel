package uz.marcos.obozrevatel.service;

import uz.marcos.obozrevatel.entity.Message;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

public interface MessageService {
    public Message savedMessages(HttpServletRequest request);

}
