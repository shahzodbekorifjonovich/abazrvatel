package uz.marcos.obozrevatel.service;

import uz.marcos.obozrevatel.entity.User;

import javax.servlet.http.HttpServletRequest;

public interface UserService {
    public User savedUser(HttpServletRequest request);
}
