package uz.marcos.obozrevatel.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.marcos.obozrevatel.entity.Comment;
import uz.marcos.obozrevatel.entity.User;
import uz.marcos.obozrevatel.model.Result;
import uz.marcos.obozrevatel.repository.CommentRepository;
import uz.marcos.obozrevatel.repository.PostRepository;
import uz.marcos.obozrevatel.repository.UserRepository;

import javax.jws.soap.SOAPBinding;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

@Service
public class CommentServiceImp implements CommentService {
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    PostRepository postRepository;
    @Autowired
    UserRepository userRepository;
    @Override
    public Result save(HttpServletRequest request, User user, String s) {
        Result result=new Result();
        Comment comment=new Comment();
        comment.setText(s.substring(5));
        comment.setUser(user);
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if(cookie.getName().equals("postId")){
                comment.setPost(postRepository.getOne(cookie.getValue()));
                break;
            }
        }

        try{
commentRepository.save(comment);
result.setSuccess(true);
        }
        catch (Exception e){

        }
        return result;
    }

}
