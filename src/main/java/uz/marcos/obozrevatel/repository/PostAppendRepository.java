package uz.marcos.obozrevatel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.marcos.obozrevatel.entity.PostAppend;

import java.util.List;

@Repository
public interface PostAppendRepository extends JpaRepository<PostAppend,Integer> {
List<PostAppend> findAllByPostId(String postId);
//List<PostAppend> findAllByOrderByIdDesc(String postId);

}
