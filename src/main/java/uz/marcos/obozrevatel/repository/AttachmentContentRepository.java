package uz.marcos.obozrevatel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.marcos.obozrevatel.entity.AttachmentContent;
@Repository
public interface AttachmentContentRepository extends JpaRepository<AttachmentContent,Integer> {
    AttachmentContent findByAttachmentId(Integer attachmentId);
}
