package uz.marcos.obozrevatel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.marcos.obozrevatel.entity.Message;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message,Integer> {
List<Message> findAllByOrderByIdDesc();
}
