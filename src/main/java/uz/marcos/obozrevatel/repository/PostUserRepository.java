package uz.marcos.obozrevatel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.marcos.obozrevatel.entity.PostUser;
@Repository
public interface PostUserRepository extends JpaRepository<PostUser,Integer> {
}
