package uz.marcos.obozrevatel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.marcos.obozrevatel.entity.Categoriya;
@Repository
public interface CategoryRepository extends JpaRepository<Categoriya,Integer> {
}
