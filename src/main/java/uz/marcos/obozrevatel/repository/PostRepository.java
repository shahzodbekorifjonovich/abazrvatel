package uz.marcos.obozrevatel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.marcos.obozrevatel.entity.Post;

import java.util.UUID;

@Repository
public interface PostRepository extends JpaRepository<Post, String> {
}
