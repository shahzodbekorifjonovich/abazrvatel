package uz.marcos.obozrevatel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.marcos.obozrevatel.entity.Comment;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment,Integer>{
    List<Comment> findAllByPostId(String id);
}
