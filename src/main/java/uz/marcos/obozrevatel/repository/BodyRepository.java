package uz.marcos.obozrevatel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.marcos.obozrevatel.entity.Body;
@Repository

public interface BodyRepository extends JpaRepository<Body,Integer> {
}
