package uz.marcos.obozrevatel.component;

import org.omg.PortableServer.POA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.marcos.obozrevatel.entity.Post;
import uz.marcos.obozrevatel.entity.PostUser;
import uz.marcos.obozrevatel.entity.Role;
import uz.marcos.obozrevatel.entity.User;
import uz.marcos.obozrevatel.repository.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class DataLoader implements CommandLineRunner {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    PostRepository postRepository;
    @Autowired
    PostUserRepository postUserRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            List<Role> adminRoles = roleRepository.findAll();
           User user= userRepository.save(new User("admin",
                    passwordEncoder.encode("12345"),
                    "mail@mail.uz", "Fullname", adminRoles));

            Post post=new Post();
            UUID uuid=UUID.randomUUID();
            post.setId(uuid.toString());
            post.setTitle("salo,ls,lmekd");

           postRepository.save(post);
             post=new Post();
            post=new Post();
            post.setTitle("salo,ls,lmekdAndijon viloyati hokimligi bir kunda yarim tonna paxta tergan ayolga Spark sovg‘a qildi (video)\n" +
                    "Andijonlik terimchi ayol bir kunda 500 kilogramm paxta terib rekord o‘rnatdi. Ushbu rekord uchun Andijon viloyat hokimligi terimchi ayolga Spark avtomobilini sovg‘a qildi, deb xabar beradi");
             postRepository.save(post);




        }
    }
}
