package uz.marcos.obozrevatel.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import uz.marcos.obozrevatel.entity.Body;

import java.io.IOException;

public class BodySerialize extends StdSerializer<Body> {

    public BodySerialize() {
        this(null);
    }

    public BodySerialize(Class<Body> t) {
        super(t);
    }

    @Override
    public void serialize(Body body, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

           jsonGenerator.writeStartObject();
           jsonGenerator.writeNumberField("id", body.getId());
           jsonGenerator.writeStringField("value", body.getValue());
           jsonGenerator.writeEndObject();
        }

}