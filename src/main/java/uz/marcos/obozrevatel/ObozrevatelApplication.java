package uz.marcos.obozrevatel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ObozrevatelApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(ObozrevatelApplication.class, args);
    }

}
