package uz.marcos.obozrevatel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "category")
public class Categoriya {
    @Id
    private Integer id;
    private String name;
}
