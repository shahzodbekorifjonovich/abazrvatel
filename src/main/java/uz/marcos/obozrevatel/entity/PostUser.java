package uz.marcos.obozrevatel.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class PostUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer user_id;
    private String post_id;
    private boolean emoji1=false;
    private boolean emoji2=false;
    private boolean emoji3=false;
    private boolean emoji4=false;
    private boolean emoji5=false;
    private boolean emoji6=false;
    private boolean likes=false;
}
