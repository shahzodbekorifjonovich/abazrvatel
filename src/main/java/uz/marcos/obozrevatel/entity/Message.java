package uz.marcos.obozrevatel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name",columnDefinition = "VARCHAR(25)")
    private String name;
    @Column(name = "text",columnDefinition = "VARCHAR(255)")
    private String text;
    @Column(name = "phone_number",columnDefinition = "character varying (13)")
    private String phoneNumber;


}
