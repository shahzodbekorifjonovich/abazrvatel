package uz.marcos.obozrevatel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Attachment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String contentType;
    private long size;
    private String originalName;
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "postId")
//    private Attachment attachment;
}
