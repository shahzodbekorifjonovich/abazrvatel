package uz.marcos.obozrevatel.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValyutaResult {
    private String  usd;
    private String eur;
    private String gbp;
    private String rub;
    private String diff1;
    private String diff2;
    private String diff3;
    private String diff4;
}
