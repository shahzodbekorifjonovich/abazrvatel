package uz.marcos.obozrevatel.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PoliticSliderResponse {
    private Integer img_id;
    private Integer body_id;
}
