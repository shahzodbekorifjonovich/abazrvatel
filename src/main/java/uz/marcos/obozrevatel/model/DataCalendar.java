package uz.marcos.obozrevatel.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DataCalendar {
private Integer day;
private String month;
private Integer year;

}
