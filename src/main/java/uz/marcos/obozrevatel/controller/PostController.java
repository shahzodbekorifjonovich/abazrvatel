package uz.marcos.obozrevatel.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.marcos.obozrevatel.entity.Body;
import uz.marcos.obozrevatel.entity.Post;
import uz.marcos.obozrevatel.entity.PostAppend;
import uz.marcos.obozrevatel.model.PoliticSliderResponse;
import uz.marcos.obozrevatel.model.Result;
import uz.marcos.obozrevatel.repository.BodyRepository;
import uz.marcos.obozrevatel.repository.PostAppendRepository;
import uz.marcos.obozrevatel.repository.PostRepository;
import uz.marcos.obozrevatel.service.AttachmentService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/post")
public class PostController {
    @Autowired
    PostRepository postRepository;
    @Autowired
    BodyRepository bodyRepository;
    @Autowired
    PostAppendRepository postAppendRepository;
    @Autowired
    AttachmentService attachmentService;

    @GetMapping("/list")
    @ResponseBody
    public List<Post> posts() {
        try {
           return postRepository.findAll();
        } catch (Exception e) {
            return null;
        }
    }

    @GetMapping("/index")
    public String adminIndexPage(){
        return "/admin/index";
    }
    @GetMapping("/addId")
    @ResponseBody
    public void addId(HttpServletResponse response){
        Cookie cookie=new Cookie("postId", UUID.randomUUID().toString());
        response.addCookie(cookie);
    }
    @GetMapping("/addPost")
    public String addPostPage(){

        return "/admin/addPost";
    }

    @PostMapping("/body/save")
    public @ResponseBody
    Result bodySave(@RequestBody String body, HttpServletRequest request){

        Result result=new Result();
        Cookie[] cookies=request.getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("postId")){
                result.setMessage(cookie.getValue());
                break;
            }
        }

        Body postBody=new Body();
        try{

            postBody.setValue(body.substring(5));
            Body savedPostBody=bodyRepository.save(postBody);
            if (savedPostBody!=null){
                PostAppend postAppend=new PostAppend();
                postAppend.setName("body");
                postAppend.setPostId(result.getMessage());
                postAppend.setAppendId(savedPostBody.getId());
                postAppendRepository.save(postAppend);
                result.setSuccess(true);
            };

        }
        catch (Exception e){

        }
        return result;
    };

    @GetMapping("/appendList/{postId}")
    @ResponseBody
    public List<PostAppend> appends(@PathVariable(value = "postId") String postId){
        try {
            System.out.println(postAppendRepository.findAllByPostId(postId));
            return postAppendRepository.findAllByPostId(postId);
        }catch (Exception e){
            return null;
        }
    }
    @ResponseBody
    @GetMapping("/postBody/{id}")
    public Body postBody(@PathVariable(value = "id") Integer id){
        Body postBody=new Body();
        try {
             postBody = bodyRepository.getOne(id);
        } catch (Exception e){
            postBody=new Body();
            System.out.println("xatolik");
        }
        return postBody;
    }
    @ResponseBody
    @GetMapping("/img/{id}")
    public byte[] getImg(@PathVariable Integer id,HttpServletResponse response){
        try{
            return attachmentService.getFile(id,response);
        }
        catch (Exception e){
            return  null;
        }
    }

    @ResponseBody
    @PostMapping("/img/save")
    public Result postImgSave(@RequestParam("file") MultipartFile files, HttpServletRequest request){
        try{
            System.out.println(attachmentService.save(files,request));
            return attachmentService.save(files,request);
        }
        catch (Exception e){
            return null;
        }
    }

    @GetMapping("/politic/slider/{postId}")
    @ResponseBody
    public PoliticSliderResponse politicSliderResponse(@PathVariable String postId){
        PoliticSliderResponse response=new PoliticSliderResponse();
        try {
            List<PostAppend> postAppends=postAppendRepository.findAllByPostId(postId);
            for (PostAppend append : postAppends) {
                if (append.getName().equals("body")&&response.getBody_id()==null){
                    response.setBody_id(append.getAppendId());
                }
                if (append.getName().equals("img")&&response.getImg_id()==null){
                    response.setImg_id(append.getAppendId());
                }
            }
            return response;
        }catch (Exception e){
            return null;
        }
    }
}
