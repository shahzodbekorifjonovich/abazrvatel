package uz.marcos.obozrevatel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.marcos.obozrevatel.entity.Comment;
import uz.marcos.obozrevatel.entity.User;
import uz.marcos.obozrevatel.model.Result;
import uz.marcos.obozrevatel.repository.CommentRepository;
import uz.marcos.obozrevatel.security.CurrentUser;
import uz.marcos.obozrevatel.service.CommentService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/comment")
public class CommentController {
@Autowired
    CommentService commentService;
@Autowired
CommentRepository commentRepository;
    @GetMapping("/isUser")
    @ResponseBody
    public boolean currentUser(@CurrentUser User user){
        if (user!=null){
            return true;
        }
        return false;

    }


@PostMapping("/save")
@ResponseBody
    public Result commentSave(HttpServletRequest request, @RequestBody String text, @CurrentUser User user){
    System.out.println(text);
    return commentService.save(request,user,text);
}
@GetMapping("/list")
    @ResponseBody
   public List<Comment> commentList(){
  List<Comment>  comments=commentRepository.findAll();
    for (Comment comment : comments) {
        comment.setPost(null);
        comment.setUser(null);
    }

    System.out.println(comments);
    return comments;
}


}
