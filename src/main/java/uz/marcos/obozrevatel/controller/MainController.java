package uz.marcos.obozrevatel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import uz.marcos.obozrevatel.service.CalendaService;
import uz.marcos.obozrevatel.service.ValyutaService;

@Controller
public class MainController {
    @Autowired
    ValyutaService valyutaService;
    @Autowired
    CalendaService calendarService;
    @GetMapping("/")
    public String Index(Model model){
        model.addAttribute("valyuta",valyutaService.getValyuta());
        model.addAttribute("data",calendarService.getData());
        return "index";
    }
}
