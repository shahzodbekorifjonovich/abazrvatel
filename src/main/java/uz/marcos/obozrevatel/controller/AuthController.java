package uz.marcos.obozrevatel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import uz.marcos.obozrevatel.entity.Message;
import uz.marcos.obozrevatel.repository.MessageRepository;
import uz.marcos.obozrevatel.repository.UserRepository;
import uz.marcos.obozrevatel.service.CalendaService;
import uz.marcos.obozrevatel.service.MessageServiceImp;
import uz.marcos.obozrevatel.service.UserServiceImp;
import uz.marcos.obozrevatel.service.ValyutaService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    ValyutaService valyutaService;
    @Autowired
    CalendaService calendarService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserServiceImp userServiceImp;
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    MessageServiceImp messageServiceImp;

    @GetMapping("/")
    public String Index(Model model) {
        model.addAttribute("valyuta", valyutaService.getValyuta());
        model.addAttribute("data", calendarService.getData());
        return "index";
    }

    @GetMapping("/business")
    public String businessPage(Model model) {
        model.addAttribute("valyuta", valyutaService.getValyuta());
        model.addAttribute("data", calendarService.getData());
        return "business";
    }

    @GetMapping("/cultura")
    public String culturaPage(Model model) {
        model.addAttribute("valyuta", valyutaService.getValyuta());
        model.addAttribute("data", calendarService.getData());
        return "culture";
    }

    @GetMapping("/mneniya")
    public String mneniyaPage(Model model) {
        model.addAttribute("valyuta", valyutaService.getValyuta());
        model.addAttribute("data", calendarService.getData());
        return "mind";
    }

    @GetMapping("/novosti")
    public String novostiPage(Model model) {
        model.addAttribute("valyuta", valyutaService.getValyuta());
        model.addAttribute("data", calendarService.getData());
        return "news";
    }

    @GetMapping("/plus18")
    public String Plus18tPage(Model model) {
        model.addAttribute("valyuta", valyutaService.getValyuta());
        model.addAttribute("data", calendarService.getData());
        return "plus18";
    }

    @GetMapping("/politic")
    public String PoliticPage(Model model) {
        model.addAttribute("valyuta", valyutaService.getValyuta());
        model.addAttribute("data", calendarService.getData());
        return "politic";
    }

    @GetMapping("/post")
    public String PostPage(Model model) {
        model.addAttribute("valyuta", valyutaService.getValyuta());
        model.addAttribute("data", calendarService.getData());
        return "post";
    }

    @GetMapping("/razbor")
    public String razborPage(Model model) {
        model.addAttribute("valyuta", valyutaService.getValyuta());
        model.addAttribute("data", calendarService.getData());
        return "parsing";
    }

    @GetMapping("/search")
    public String searchPage(Model model) {
        model.addAttribute("valyuta", valyutaService.getValyuta());
        model.addAttribute("data", calendarService.getData());
        return "search_page";
    }

    @PostMapping("/signup")
    public String saveUser(HttpServletRequest request, Model model) {
        model.addAttribute("valyuta", valyutaService.getValyuta());
        model.addAttribute("data", calendarService.getData());
        if (userServiceImp.savedUser(request) != null) {
            return "post";
        } else {
            return "post";
        }
    }

    @PostMapping("/message")
    public String sendMessage(HttpServletRequest request) {
        Message message = new Message();
        message = messageServiceImp.savedMessages(request);
        if (message != null) {
            return "redirect:/auth/";
        } else {
            return "index";
        }
    }
    @GetMapping("/messageList")
    public String messageListPage(){
        return "admin/listMessages";
    }
    @GetMapping("/listMessages")
    @ResponseBody
    public List<Message> list(){
        List<Message>messages=messageRepository.findAllByOrderByIdDesc();
      return messages;
    }


}