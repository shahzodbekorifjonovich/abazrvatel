$(window).load(function () {
    // if (!window.localStorage.getItem('preloaderIsShown')) {
    //     setTimeout(function() {
    //         $('.shimmer').fadeOut('slow');
    //         window.localStorage.setItem('preloaderIsShown', true);
    //     }, 2500);
    // } else {
    //     $('.shimmer').css('display', 'none');
    // }

    $('.shimmer').fadeOut('slow');

});

// $( document ).ready(function() {
//     $('img').on('load', function(){
//         $('.shimmer').fadeOut('slow');
//     });
// });


$('.comment .users .item .sms span img').click(function () {
    $(this).parents('span').toggleClass('liked');
    if ($(this).parents('span').hasClass('liked')) {
        $(this).attr('src', 'img/heart_red.svg')
    } else {
        $(this).attr('src', 'img/heart.svg')
    }
});

$(function () {
    $('.marquee').marquee({
        pauseOnHover: true,
        speed: 50,
        direction: 'right',
        duplicated: true,
        startVisible: true,
        pauseOnCycle: true

    })
});


$('.others').flickity({
    cellAlign: 'left',
    contain: true,
    wrapAround: true,
    freeScroll: true,
    groupCells: '100%'
});

$('.right_fixed .social').flickity({
    cellAlign: 'left',
    pageDots: false,
    prevNextButtons: false,
    autoPlay: 2000,
    pauseAutoPlayOnHover: true,
    fade: true
});

$('.politic_slider .cards').flickity({
    pageDots: false,
    wrapAround: true
});

$(".report").click(function (event) {
    $('.modal_report').addClass('active');
});
$(".post .submit").click(function (event) {
    $('.modal_sign_in').addClass('active');
});
$('.modal .close').click(function (event) {
    $('.modal').removeClass('active');
});
$('.modal .bg').mouseup(function () {
    $('.modal').removeClass('active');
});
$('.modal_sign_in .register').click(function () {
    $('.modal_sign_in').removeClass('active');
    $('.modal_sign_up').addClass('active')
});

$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 80
    }, 1000);
});

$(window).scroll(function () {
    if ($(window).scrollTop() > 155) {
        $(".right_fixed .arrow").addClass('show');
        $('header .main').addClass('fixed')
    }
    else {
        $(".right_fixed .arrow").removeClass('show');
        $('header .main').removeClass('fixed');
    }
});

$('.search > a').click(function () {
    $('.search').addClass('active');
    $('.search .input_block input').focus();
});
$('.search .input_block .close').click(function () {
    $('.search').removeClass('active')
});

$('.modal_sign_in .line input').click(function () {
    $(this).parent('.line').addClass('focus');
});

$('.modal_sign_up .line input').click(function () {
    $(this).parent('.line').addClass('focus')
});

$('form .line .hide a').click(function () {
    $(this).fadeOut('100').siblings('a').fadeIn('100');
    $(this).parent('.hide').toggleClass('clicked');

    if ($(this).parent('.hide').hasClass('clicked')) {
        $("#password").attr("type", "text");
    } else {
        $("#password").attr("type", "password");
    }
});

$('form .line .hide1 a').click(function () {
    $(this).fadeOut('100').siblings('a').fadeIn('100');
    $(this).parent('.hide1').toggleClass('clicked');

    if ($(this).parent('.hide1').hasClass('clicked')) {
        $("#select_password").attr("type", "text");
    } else {
        $("#select_password").attr("type", "password");
    }
});

$('form .line .hide2 a').click(function () {
    $(this).fadeOut('100').siblings('a').fadeIn('100');
    $(this).parent('.hide2').toggleClass('clicked');

    if($(this).parent('.hide2').hasClass('clicked')){
        $("#select_password2").attr("type","text");
    }else{
        $("#select_password2").attr("type","password");
    }
});

