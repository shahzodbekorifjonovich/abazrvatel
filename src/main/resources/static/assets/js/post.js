$(document).ready(function () {

    var postTable = $('#postTable').DataTable({


        ajax:{
            url: "/post/list",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataSrc: '',
        },
        columns: [
            // {title: "User Id", data: "userId"},
            {title: "Title", data: "title"},
            // {title: "Body", data: "body"},
            {
                title: "Action", data: "id", render: function (data, type, row) {
                    return '<div class="row">\n' +
                        '    <div class="col">\n' +
                        '        <button  type="button" class="btn btn-primary text-white btn-block btn-edit"><i class="fas fa-edit"></i> Edit</button>\n' +


                        '        <button  class=" text-white btn btn-danger btn-block btn-delete" style="color: #2c5c8b"><i class="fab fa-bitbucket"> </i> Delete</button>\n' +
                        '    </div>\n' +
                        '</div>\n';
                }
            }
        ],
    });

    $('.addPostBt').click(function () {
        $.ajax({
            url: "/post/addId",
            contentType: "application/json; charset=utf-8",
            type: "GET",
        })
    })

})
// $('.ddd').click(function ccc(){
//     alert("sasasa")
//     $.ajax({
//         url: "/auth/post/list",
//         contentType: "application/json; charset=utf-8",
//         type: "GET",
//         success:function (data) {
//             alert("ajax")
//             console.log(data)
//         }
//     }).error(function () {
//         alert("xato")
//     })
// })
