$(document).ready(function () {
    function postIndex(postID) {
        $.ajax({
            url: "/post/"+postID,
            contentType: "application/json; charset=utf-8",
            method: "GET",
            success:function (data) {
                var postChildern= $('.postChildern');
                postChildern.html("");
                data.forEach(function (d) {
                    if (d.name==='body'){
                            var bodyValue;
                        function f(res) {
                            bodyValue=res;
                            console.log(res)
                        }

                        $.ajax({
                            url: "/post/postBody/"+d.appendId,
                            contentType: "application/json; charset=utf-8",
                            type:'json',
                            method: "GET",
                            success:function (data) {
                                f(data.value);
                                // bodyValue=data.value;
                            }

                        })
                        var bodyDiv=$('<div >'+bodyValue+'</div>')
                        postChildern.append(bodyDiv);

                    }
                    else if (d.name==='img'){
                        var imgDiv=$('<img src="/post/img/'+d.appendId+'" class="img-fluid" alt="rasm">')
                        postChildern.append(imgDiv);
                    }
                })
            }
        })
    }

})