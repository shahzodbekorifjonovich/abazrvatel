$(document).ready(function () {
    function postChildern(postId) {
        $.ajax({
            url: "/post/appendList/"+postId,
            contentType: "application/json; charset=utf-8",
            method: "GET",
            success:function (data) {
                var postChildern= $('.postChildern');
                postChildern.html("");
                data.forEach(function (d) {
                    if (d.name==='body'){
                        var bodyValue;
                        function f(res) {
                            bodyValue=res;
                            console.log(res)
                        }

                        $.ajax({
                            url: "/post/postBody/"+d.appendId,
                            contentType: "application/json; charset=utf-8",
                            type:'json',
                            method: "GET",
                            success:function (data) {
                                f(data.value);
                                // bodyValue=data.value;
                            }

                        })
                        var bodyDiv=$('<div >'+bodyValue+'</div>')
                        postChildern.append(bodyDiv);

                    }
                    else if (d.name==='img'){
                        var imgDiv=$('<img src="/post/img/'+d.appendId+'" class="img-fluid" alt="rasm">')
                        postChildern.append(imgDiv);
                    }
                })
            }
        })
    }

    $('.addBodySaveBt').click(function(){
        var form=$('#bodyForm');
        console.log(form.serialize())
        $.ajax({
            url: "/post/body/save",
            contentType: "application/json; charset=utf-8",
            dataType:'json',
            method: "POST",
            data:form.serialize(),
            success:function (data) {

                if (data.success){
                    postChildern(data.message);
                }
                $('#bodyForm')[0].reset();
                $('#addbody').modal('toggle');

            }
        })
    })

    $('.addImgSaveBt').click(function(){
        var form=$('#imgForm')[0];
        var data=new FormData(form);

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "/post/img/save",
            data: data,
            //http://api.jquery.com/jQuery.ajax/
            //https://developer.mozilla.org/en-US/docs/Web/API/FormData/Using_FormData_Objects
            processData: false, //prevent jQuery from automatically transforming the data into a query string
            contentType: false,
            cache: false,
            success:function (data) {
                if (data.success){
                    $('#addimg').modal('toggle');
                    postChildern(data.message);
                }
            }
        })
    })

})
