$('.big_cards').flickity({
    cellAlign: 'left',
    contain: true,
    prevNextButtons: false
});

$('.popular_cards .big').flickity({
    cellAlign: 'left',
    contain: true,
    wrapAround: true,
    freeScroll: true,
    arrowShape: 'M 10,50 L 60,100 L 60,90 L 60,50  L 60,10 L 60,0 Z'
});

$('.popular_cards .small').flickity({
    cellAlign: 'left',
    contain: true,
    pageDots: false,
    wrapAround: true,
    freeScroll: true
});

$('.small_slider').flickity({
    cellAlign: 'left',
    contain: true,
    pageDots: false,
    wrapAround: true,
    freeScroll: true
});

$('.comment .users .item .sms span img').click(function () {
    $(this).parents('span').toggleClass('liked');
    if ($(this).parents('span').hasClass('liked')) {
        $(this).attr('src', 'img/heart_red.svg')
    } else {
        $(this).attr('src', 'img/heart.svg')
    }
});

$(".report").click(function (event) {
    $('.modal_report').addClass('active');
});
$(".comments form .submit").click(function (event) {
    $('.modal_sign').addClass('active');
});
$('.modal .close').click(function (event) {
    $('.modal').removeClass('active');
});
$('.modal .bg').mouseup(function () {
    $('.modal').removeClass('active');
});

$(function () {
    $('.marquee').marquee({
        pauseOnHover: true,
        speed: 30,
        direction: 'right',
        duplicated: true,
        startVisible: true,
        pauseOnCycle: true

    })
});

$('.search > a').click(function () {
    $('.search').addClass('active');
    $('.search .input_block input').focus();
});
$('.search .input_block .close').click(function () {
    $('.search').removeClass('active')
});

$('header .menu_icon').click(function () {
    $('header .menu_icon a').toggleClass('hide');
    $('header .menu_icon img').toggleClass('show');
    $('.menu').toggleClass('show');
    $('header .main').toggleClass('top-0')
});

$(window).scroll(function() {
    if($(window).scrollTop() > 42){
        $('header .main').addClass('fixed');
    }
    else{
        $('header .main').removeClass('fixed');
    }
});

$('.modal_sign .tabs a').click(function () {
    $('.modal_sign .tabs a').removeClass('active');
    $('.modal_sign form').removeClass('active');
    $(this).addClass('active');
    $('.modal_sign form[data-tab=' + $(this).data("tab-open") + ']').addClass('active');
});

$('.modal_sign .line input').click(function () {
    $(this).parent('.line').addClass('focus')
});

$('form .line .hide a').click(function () {
    $(this).fadeOut('100').siblings('a').fadeIn('100');
    $(this).parent('.hide').toggleClass('clicked');

    if ($(this).parent('.hide').hasClass('clicked')) {
        $("#password").attr("type", "text");
    } else {
        $("#password").attr("type", "password");
    }
});

$('form .line .hide1 a').click(function () {
    $(this).fadeOut('100').siblings('a').fadeIn('100');
    $(this).parent('.hide1').toggleClass('clicked');

    if ($(this).parent('.hide1').hasClass('clicked')) {
        $("#select_password").attr("type", "text");
    } else {
        $("#select_password").attr("type", "password");
    }
});

$('form .line .hide2 a').click(function () {
    $(this).fadeOut('100').siblings('a').fadeIn('100');
    $(this).parent('.hide2').toggleClass('clicked');

    if($(this).parent('.hide2').hasClass('clicked')){
        $("#select_password2").attr("type","text");
    }else{
        $("#select_password2").attr("type","password");
    }
});